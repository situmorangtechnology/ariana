package main

import (
	"context"
	"database/sql"
	"fmt"
	"net/url"
	"os"
	"os/signal"
	"time"

	"github.com/go-playground/validator/v10"
	_ "github.com/go-sql-driver/mysql"
	"github.com/golang-migrate/migrate"
	"github.com/golang-migrate/migrate/database/mysql"
	_ "github.com/golang-migrate/migrate/source/file"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"

	articleHandler "bitbucket.org/situmorangtechnology/ariana/article/http"
	articleRepository "bitbucket.org/situmorangtechnology/ariana/article/repository/mysql"
	articleUsecase "bitbucket.org/situmorangtechnology/ariana/article/usecase"
	mw "bitbucket.org/situmorangtechnology/ariana/middleware"
	"bitbucket.org/situmorangtechnology/ariana/models"
)

func init() {
	viper.SetConfigFile(`configs/config.json`)
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}

	if viper.GetBool("debug") {
		log.SetLevel(log.DebugLevel)
		log.Warn("service is running in DEBUG Mode")
		return
	}
	log.SetLevel(log.InfoLevel)
	log.Info("service is running in PRODUCTION Mode")
}

func main() {
	dbHost := viper.GetString(`database.host`)
	dbPort := viper.GetString(`database.port`)
	dbUser := viper.GetString(`database.user`)
	dbPass := viper.GetString(`database.pass`)
	dbName := viper.GetString(`database.name`)
	connection := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", dbUser, dbPass, dbHost, dbPort, dbName)
	val := url.Values{}
	val.Add("parseTime", "1")
	val.Add("loc", "Asia/Jakarta")
	dsn := fmt.Sprintf("%s?%s", connection, val.Encode())
	dbConn, err := sql.Open(`mysql`, dsn)

	if err != nil {
		log.Fatal(err)
	}
	err = dbConn.Ping()
	if err != nil {
		log.Fatal(err)
	}

	defer func() {
		err := dbConn.Close()
		if err != nil {
			log.Fatal(err)
		}
	}()

	// Migration
	driver, err := mysql.WithInstance(dbConn, &mysql.Config{})
	if err != nil {
		log.Fatal(err)
	}
	m, err := migrate.NewWithDatabaseInstance("file://migrations/", "mysql", driver)
	if err != nil {
		log.Fatal(err)
	}
	_ = m.Up()

	// Server
	e := echo.New()
	e.HideBanner = true
	e.Use(middleware.Recover())
	e.Use(middleware.Logger())
	e.Use(mw.ContextTimeout(time.Duration(viper.GetInt("context.timeout")) * time.Second))
	e.Use(mw.ErrMiddleware())
	e.Validator = &models.CustomValidator{Validator: validator.New()}

	// Domain
	ar := articleRepository.NewMysqlRepository(dbConn)
	au := articleUsecase.NewArticleUsecase(ar)
	articleHandler.NewHandler(e, au)

	// Start server
	go func() {
		if err := e.Start(viper.GetString("server.address")); err != nil {
			e.Logger.Info("shutting down the server...")
		}
	}()

	// Wait for interrupt signal to gracefully shutdown the server with
	// a timeout of 10 seconds.
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	if err := e.Shutdown(ctx); err != nil {
		e.Logger.Fatal(err)
	}
}
