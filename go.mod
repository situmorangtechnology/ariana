module bitbucket.org/situmorangtechnology/ariana

go 1.14

require (
	github.com/Masterminds/squirrel v1.4.0
	github.com/go-playground/validator/v10 v10.3.0
	github.com/go-sql-driver/mysql v1.5.0
	github.com/golang-migrate/migrate v3.5.4+incompatible
	github.com/google/uuid v1.1.1
	github.com/labstack/echo/v4 v4.1.16
	github.com/sirupsen/logrus v1.6.0
	github.com/spf13/viper v1.7.0
	github.com/stretchr/testify v1.4.0
)
